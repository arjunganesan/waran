﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WARAN.Hub.Master.Common.Utils
{
    public static class Temperature
    {
        public static double CelsiusToFahrenheit(double c)
        {
            return (c * (9 / 5)) + 32;
        }
        public static double FahrenheitToCelsius(double f)
        {
            return (f - 32) * (5 / 9);
        }
    }
}
