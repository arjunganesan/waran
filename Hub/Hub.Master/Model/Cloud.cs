﻿using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PubNubMessaging.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WARAN.Hub.Master.Model
{
    public class Cloud
    {

        private static Pubnub pubnub = new Pubnub(Common.Config.PUBNUB_PUBLISHER_KEY, Common.Config.PUBNUB_SUBSCRIBER_KEY);
        public static MobileServiceClient MobileService = new MobileServiceClient(Common.Config.MOBILE_SERVICE_URI, Common.Config.MOBILE_SERVICE_APP_KEY);

        public delegate void CloudEventHandler(object sender, CloudEventArgs e);

        public static event CloudEventHandler CloudChanged;

        static Cloud()
        {
            //Subscribe to the action channel so whenever the phone sends some command the userCallback will be executed
            pubnub.Subscribe<string>(Common.Config.PUBNUB_ACTION_CHANNEL, (publishResult) => {
                //Data comes from PubNub via Azure Mobile Service
                JArray message = JArray.Parse(publishResult);
                CloudObject cObj = JsonConvert.DeserializeObject<CloudObject>(message[0].ToString()); //Deserialize and get the address of the module and what command it should execute
                CloudChanged(null, new CloudEventArgs() { Data  = cObj }); //Raise the cloud changed event with the data obtained form pubnub
            }, (publishResult) => {
                Debug.WriteLine(publishResult);
            }, (error) => {
                Debug.WriteLine(error);
            });
        }

        public static async Task<T> Add<T>(T Data)
        {
            await MobileService.GetTable<T>().InsertAsync(Data); //Add a module in the cloud
            return Data;
        }

        public static async Task<T> Save<T>(T Data)
        {
            await MobileService.GetTable<T>().UpdateAsync(Data); //Update module details in cloud
            return Data;
        }

        public static async void Delete<T>(T Data)
        {
            await MobileService.GetTable<T>().DeleteAsync(Data); //Remove a module from cloud
        }

        public static async void Send<T>(string ApiName, T Data)
        {
            await MobileService.InvokeApiAsync<T, JObject>(ApiName, Data, HttpMethod.Post, null); //Send some data to Azure Mobiel Api
        }

        public class CloudEventArgs: EventArgs
        {
            public CloudObject Data
            {
                get; set;
            }

            public CloudEventArgs(): base()
            {

            }
        }

        public class CloudObject
        {
            [JsonProperty("address")]
            public string Address
            {
                get; set;
            }

            [JsonProperty("moduleType")]
            public Model.Modules.ModuleType ModuelType
            {
                get; set;
            }

            [JsonProperty("command")]
            public string Command
            {
                get; set;
            }

        }

    }

}
