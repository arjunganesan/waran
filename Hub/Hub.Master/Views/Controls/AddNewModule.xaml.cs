﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using WARAN.Hub.Master.Model.Modules;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace WARAN.Hub.Master.Views.Controls
{
    /// <summary>
    /// Usercontrol where we give new module detaisl that are to added to the hub
    /// </summary>
    public sealed partial class AddNewModule : UserControl
    {

        ViewModel.ViewModelLocator locator;

        public AddNewModule()
        {
            this.InitializeComponent();
            this.Loaded += AddNewModule_Loaded;
        }

        private void AddNewModule_Loaded(object sender, RoutedEventArgs e)
        {
            locator = this.Resources["ViewModelLocator"] as ViewModel.ViewModelLocator; //Getting a reference to view model locator
        }

        /// <summary>
        /// Adding the new module to hub
        /// </summary>
        private void Save_Tapped(object sender, TappedRoutedEventArgs e)
        {
            switch (ModuleType.SelectedIndex)
            {
                case 0:
                    Model.Modules.PlantWaterer plantWaterer = new PlantWaterer(Guid.NewGuid().ToString(), ModuleName.Text);
                    plantWaterer.Address = ModuleAddress.Text;
                    locator.Main.AddModule<Model.Modules.PlantWaterer>(plantWaterer);
                    break;
                case 1:
                    Model.Modules.TemperatureMonitor temperatureMonitor = new TemperatureMonitor(Guid.NewGuid().ToString(), ModuleName.Text);
                    temperatureMonitor.Address = ModuleAddress.Text;
                    locator.Main.AddModule<Model.Modules.TemperatureMonitor>(temperatureMonitor);
                    break;
                case 2:
                    break;
                case 3:
                    break;
            }
        }

        /// <summary>
        /// Closes the control
        /// </summary>
        private void Cancel_Tapped(object sender, TappedRoutedEventArgs e)
        {
            locator.Main.ToggleAddNewCommand.Execute(null);
        }
    }
}
