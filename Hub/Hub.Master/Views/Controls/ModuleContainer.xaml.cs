﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using WARAN.Hub.Master.Model;
using WARAN.Hub.Master.Model.Modules;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace WARAN.Hub.Master.Views.Controls
{
    /// <summary>
    /// User control which will load control specific to the module type passed to it.
    /// In module list page we add this control in item tmeplate of itemscontrol which lists all module
    /// This will load teh appropriate user control based on the type of the module passed to it
    /// </summary>
    public sealed partial class ModuleContainer : UserControl
    {

        public static readonly DependencyProperty ModuleIdProperty = DependencyProperty.Register("ModuleId", typeof(String), typeof(ModuleContainer), new PropertyMetadata(null));
        /// <summary>
        /// Id of the module that should be dsiplayed
        /// </summary>
        public String ModuleId
        {
            get
            {
                return (String)GetValue(ModuleIdProperty);
            }
            set
            {
                SetValue(ModuleIdProperty, value);
            }
        }

        public static readonly DependencyProperty ModuleTypeProperty = DependencyProperty.Register("ModuleType", typeof(ModuleType), typeof(ModuleContainer), new PropertyMetadata(null));
        /// <summary>
        /// Type of the module that should be displayed
        /// </summary>
        public ModuleType? ModuleType
        {
            get
            {
                if(GetValue(ModuleTypeProperty) == null)
                {
                    return null;
                }
                return (ModuleType)GetValue(ModuleTypeProperty);
            }
            set
            {
                SetValue(ModuleTypeProperty, value);
            }
        }

        ViewModel.ViewModelLocator locator;

        public ModuleContainer()
        {
            this.InitializeComponent();
            this.Loaded += ModuleContainer_Loaded;
        }

        private void ModuleContainer_Loaded(object sender, RoutedEventArgs e)
        {
            locator = App.Current.Resources["ViewModelLocator"] as ViewModel.ViewModelLocator; //Get a reference to the view model locator
            //Based on the module type load the appropriate user control
            switch (ModuleType)
            {
                case Model.Modules.ModuleType.PlantWaterer:
                    PlantWaterer plantWaterer = (PlantWaterer)locator.Main.Modules.FirstOrDefault(p => p.Id == ModuleId); //Getting the module using module id
                    Container.Children.Add(new Controls.Modules.PlantWaterer(plantWaterer)); //Initiating the plant waterer user control with the module data and adding it to the container
                    break;
                case Model.Modules.ModuleType.TemperatureMonitor:
                    TemperatureMonitor temperatureMonitor = (TemperatureMonitor)locator.Main.Modules.FirstOrDefault(p => p.Id == ModuleId); //Getting the module using module id
                    Container.Children.Add(new Controls.Modules.TemperatureMonitor(temperatureMonitor)); //Initiating the temperature monitor user control with the module data and adding it to the container
                    break;
            }
        }
    }
}
