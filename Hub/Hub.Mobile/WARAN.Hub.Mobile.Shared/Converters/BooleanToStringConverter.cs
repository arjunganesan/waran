﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml.Data;

namespace WARAN.Hub.Mobile.Converters
{
    public class BooleanToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            String[] parameters = parameter.ToString().Split('|');
            if(value == null)
            {
                return parameters[0];
            }
            if ((Boolean)value)
            {
                return parameters[1];
            }
            else
            {
                return parameters[0];
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }
    }
}
