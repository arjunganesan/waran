﻿using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PubNubMessaging.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WARAN.Hub.Mobile.Model
{
    public class Cloud
    {

        private static Pubnub pubnub = new Pubnub(Common.Config.PUBNUB_PUBLISHER_KEY, Common.Config.PUBNUB_SUBSCRIBER_KEY);
        public static MobileServiceClient MobileService = new MobileServiceClient(Common.Config.MOBILE_SERVICE_URI, Common.Config.MOBILE_SERVICE_APP_KEY);

        public delegate void CloudEventHandler(object sender, CloudEventArgs e);

        public static event CloudEventHandler CloudChanged;

        static Cloud()
        {
            //Subscribe to data channel to get notification whenever the data in mobile service changes
            pubnub.Subscribe<string>(Common.Config.PUBNUB_DATA_CHANNEL, (publishResult) => {
                JArray message = JArray.Parse(publishResult);
                CloudChanged(null, new CloudEventArgs() { Data  = message[0] });
            }, (publishResult) => {
                Debug.WriteLine(publishResult);
            }, (error) => {
                Debug.WriteLine(error);
            });
        }

        public static async Task<T> Add<T>(T Data)
        {
            await MobileService.GetTable<T>().InsertAsync(Data);
            return Data;
        }

        public static async Task<T> Save<T>(T Data)
        {
            await MobileService.GetTable<T>().UpdateAsync(Data);
            return Data;
        }

        public static async void Delete<T>(T Data)
        {
            await MobileService.GetTable<T>().DeleteAsync(Data);
        }

        public static async void Send<T>(string ApiName, T Data)
        {
            await MobileService.InvokeApiAsync<T, JObject>(ApiName, Data, HttpMethod.Post, null);
        }

        public static async Task<List<T>> GetAll<T>()
        {
            return await MobileService.GetTable<T>().ToListAsync();
        }
        
        public static async Task<List<Model.MyModule>> GetMyModules()
        {
            return await GetAll<Model.MyModule>();
        }

        public static async Task<List<Model.Modules.BaseModule>> GetModules()
        {
            List<Model.Modules.BaseModule> modules = new List<Modules.BaseModule>();
            modules.AddRange(await GetAll<Model.Modules.PlantWaterer>());
            modules.AddRange(await GetAll<Model.Modules.TemperatureMonitor>());
            return modules;
        }

        public class CloudEventArgs: EventArgs
        {
            public JToken Data
            {
                get; set;
            }

            public CloudEventArgs(): base()
            {

            }
        }


        /// <summary>
        /// Registering the device with azure for push notification
        /// </summary>
        public async static void UploadChannel()
        {
            var channel = await Windows.Networking.PushNotifications.PushNotificationChannelManager.CreatePushNotificationChannelForApplicationAsync();
            try
            {
                await MobileService.GetPush().RegisterNativeAsync(channel.Uri);
            }
            catch (Exception exception)
            {
                
            }
        }

        public class CloudObject
        {
            [JsonProperty("address")]
            public string Address
            {
                get; set;
            }

            [JsonProperty("moduleType")]
            public Model.Modules.ModuleType ModuelType
            {
                get; set;
            }

            [JsonProperty("action")]
            public string Action
            {
                get; set;
            }

        }

    }

}
