using GalaSoft.MvvmLight;
using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using WARAN.Hub.Mobile.Model;
using Microsoft.WindowsAzure.MobileServices;
using System.Linq;
using System.Diagnostics;
using GalaSoft.MvvmLight.Threading;

namespace WARAN.Hub.Mobile.ViewModel
{
    public class MainViewModel : ViewModelBase
    {

        private ObservableCollection<Model.MyModule> myModules;
        public ObservableCollection<Model.MyModule> MyModules
        {
            get
            {
                return myModules;
            }
            set
            {
                Set(() => MyModules, ref myModules, value);
            }
        }

        private ObservableCollection<Model.Modules.BaseModule> modules;
        public ObservableCollection<Model.Modules.BaseModule> Modules
        {
            get
            {
                return modules;
            }
            set
            {
                Set(() => Modules, ref modules, value);
            }
        }

        public async void GetAllData()
        {
            Modules = new ObservableCollection<Model.Modules.BaseModule>(await Cloud.GetModules());
            MyModules = new ObservableCollection<MyModule>(await Cloud.GetMyModules());
        }

        public MainViewModel()
        {
            
        }

    }
}