﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using WARAN.Hub.Mobile.ViewModel;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace WARAN.Hub.Mobile.Views.Controls.Modules
{
    public sealed partial class PlantWaterer : UserControl
    {
        public Model.Modules.PlantWaterer CurrentData = null;

        ViewModelLocator locator = null;
        
        public const String COMMAND_START_PUMP = "START_PUMP";
        public const String COMMAND_STOP_PUMP = "STOP_PUMP";
        public const String COMMAND_ENABLE_AUTO_WATER = "ENABLE_AUTO_WATER";
        public const String COMMAND_DISABLE_AUTO_WATER = "DISABLE_AUTO_WATER";

        public PlantWaterer()
        {
            this.InitializeComponent();
        }

        public PlantWaterer(Model.Modules.PlantWaterer Data)
        {
            this.DataContext = this.CurrentData = Data;
            this.InitializeComponent();
            this.Loaded += PlantWaterer_Loaded;
        }
        
        private void PlantWaterer_Loaded(object sender, RoutedEventArgs e)
        {
            locator = App.Current.Resources["Locator"] as ViewModelLocator;
        }

        private void TogglePump_Click(object sender, RoutedEventArgs e)
        {
            CurrentData.PumpRunning = !CurrentData.PumpRunning;
            Model.Cloud.Send<object>("plantwaterer", new { command = CurrentData.PumpRunning ? COMMAND_START_PUMP : COMMAND_STOP_PUMP, address = CurrentData.Address });
        }
    }
}
