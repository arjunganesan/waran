function insert(item, user, request) {
    request.execute();
    //Addin the data to cloud db and sending information to mobile app that data has been added in backend via PubNub 
    var pubnub = require("pubnub")({
        ssl           : true,  // <- enable TLS Tunneling over TCP
        publish_key   : process.env.PUBNUB_PUB_KEY,
        subscribe_key : process.env.PUBNUB_SUB_KEY
    });
    pubnub.publish({ 
        channel   : process.env.PUBNUB_DATA_CHANNEL, //All data related information will go through data channel of pubnub
        message   : { method: "ADD", data: item },
        callback  : function(e) { console.log( "SUCCESS!", e ); },
        error     : function(e) { console.log( "FAILED! RETRY PUBLISH!", e ); }
    });
}