#include <SPI.h>
#include "DHT.h"
#include <EEPROM.h>

#include "nRF24L01.h"
#include "RF24.h"

#define COMMAND_SIZE 32

#define DHTPIN 5
#define DHTTYPE DHT11

//All the commands possible in this module
#define COMMAND_READ_TEMPERATURE "READ_TEMPERATURE"
#define COMMAND_START_THERMO "START_THERMO"
#define COMMAND_STOP_THERMO "STOP_THERMO"
#define THERMO_PIN 6

#define CE_PIN 7
#define CSN_PIN 8

DHT dht(DHTPIN, DHTTYPE);
RF24 radio(CE_PIN, CSN_PIN);

//Address of the current module.
//Make this unique for every module.
//This is what the hub uses to identify the module.
//This is address that is given in the add new module dialog.
//Also make sure the address is exactly 9 characters long
byte moduleAddress[10] = "TMPM32476";
byte hubAddress[10] = "WARANADDR"; //Radio address of the hub

char command[32];

int temperature, humidity, feelsLike;
int isDeviceOn = 0;

byte pipeNo;

void setup() {
    Serial.begin(57600);
    radio.begin();
    
    radio.enableAckPayload();                     // Allow optional ack payloads
    radio.enableDynamicPayloads();                // Ack payloads are dynamic payloads

    radio.openWritingPipe(hubAddress);
    radio.openReadingPipe(1,moduleAddress);
    
    dht.begin();
    pinMode(THERMO_PIN, OUTPUT);
    
    radio.startListening();
    readData();
}

void loop() {
    bool gotData = false;
    while(radio.available(&pipeNo)){ //Check if radio is available
        readData(); //Read current sensor data
        radio.read(&command, COMMAND_SIZE); //Read data sent from hub
        gotData = true;
        if(String(command).equals(COMMAND_READ_TEMPERATURE)){
            sendResponse();
        }
        else if(String(command).equals(COMMAND_START_THERMO)){
            isDeviceOn = 1;
            digitalWrite(THERMO_PIN, HIGH); //Start the device
            sendResponse();
        }
        else if(String(command).equals(COMMAND_STOP_THERMO)){
            isDeviceOn = 0;
            digitalWrite(THERMO_PIN, LOW); //Stop the device
            sendResponse();
        }
    }
}

void sendResponse(){
    //Send response in the format <temperature>|<humidity>|<feelsLike>|<isDeviceOn>
    String response = String(temperature) + "|" + String(humidity) + "|" + String(feelsLike) + "|" + String(isDeviceOn);
    char responseBuffer[COMMAND_SIZE];
    response.toCharArray(responseBuffer, response.length() + 1);
    Serial.println(responseBuffer);
    radio.writeAckPayload(pipeNo, &responseBuffer, COMMAND_SIZE);
    radio.stopListening();
    radio.write(&responseBuffer, COMMAND_SIZE); //Send the composed response back to hub
    radio.startListening();
}

void readData(){
    temperature = dht.readTemperature();
    humidity = dht.readHumidity();
    feelsLike = dht.computeHeatIndex(temperature, humidity, false);
}
